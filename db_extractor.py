import numpy as np
import neo4j_connector

neo4j_connector.app.driver.verify_connectivity()

# STEP1.1: 获取原方案与走廊连接的户型内的模块
query_1 = ("MATCH (x:Corridor_Module)-[r:ACCESSES_1]-(y)"
           "WHERE y:Basic_Module|Conjunctive_Module "
           "RETURN y.global_id, r.door_family_type")
records_1, summary_1, keys_1 = neo4j_connector.app.driver.execute_query(query_1, routing_="r", database_="neo4j")

start_modules_1 = []
for record in records_1:
    start_modules_1.append([record['y.global_id'], record['r.door_family_type']])

# STEP1.2: 获取新方案与走廊连接的户型内的模块
query_2 = ("MATCH (x:Corridor_Module)-[r:ACCESSES_2]-(y)"
           "WHERE y:Basic_Module|Conjunctive_Module "
           "RETURN y.global_id, r.door_family_type ")
records_2, summary_2, keys_2 = neo4j_connector.app.driver.execute_query(query_2, routing_="r", database_="neo4j")

start_modules_2 = []
for record in records_2:
    start_modules_2.append([record['y.global_id'], record['r.door_family_type']])

# STEP2.1.1: 根据与走廊连接的户型内的模块获取原方案所有户型内所有模块及模块内的建筑构件（如果有的话）
houses_1 = []
for start_module in start_modules_1:
    query_3 = ("MATCH p = (x {global_id:$global_id})-[r:ACCESSES_1*0..]-(y) "
               "WHERE y:Basic_Module|Conjunctive_Module and NOT 'corridor_module' IN [a IN nodes(p)|a.type_name] "
               "RETURN [a IN nodes(p)|a.global_id] AS global_id, "
               "[a IN nodes(p)|a.components] AS components_a, "
               "[b IN relationships(p)|b.components] AS components_b")

    records_3, summary_3, keys_3 = neo4j_connector.app.driver.execute_query(query_3,
                                                                            global_id=start_module[0],
                                                                            routing_="r",
                                                                            database_="neo4j")
    global_ids = []
    components_all = []
    components_unit = []
    for record in records_3:
        global_ids += record["global_id"]
        for component in record["components_a"]:
            components_all += component
        for component in record["components_b"]:
            components_all += component
    components_all = np.unique(components_all)

    for x in components_all:
        components_unit.append(x)

    basic_module = list(filter(lambda module: module in neo4j_connector.basic_ids, np.unique(global_ids)))[0]
    dictionary = {'house_modules': np.unique(global_ids),
                  'basic_module': basic_module,
                  'house_components': components_unit,
                  'house_door': start_module[1]}
    houses_1.append(dictionary)

# STEP 2.1.2: 将有构件的户型和没有构件的户型分开
houses_1_source = []
houses_1_objective = []

for h in houses_1:
    if len(h['house_components']) == 0:
        dictionary_1 = {'house_modules': h['house_modules'],
                        'basic_module': h['basic_module'],
                        'house_door': h['house_door'],
                        'floor_name': neo4j_connector.source_floor_name}
        houses_1_objective.append(dictionary_1)
    else:
        houses_1_source.append(h)

# STEP2.2: 根据与走廊连接的户型内的模块获取新方案所有户型内所有模块
houses_2_objective = []
for start_module in start_modules_2:
    query_4 = ("MATCH p = (x {global_id:$global_id})-[r:ACCESSES_2*0..]-(y) "
               "WHERE y:Basic_Module|Conjunctive_Module and NOT 'corridor_module' IN [a IN nodes(p)|a.type_name] "
               "RETURN [a IN nodes(p)|a.global_id] AS global_id ")

    records_4, summary_4, keys_4 = neo4j_connector.app.driver.execute_query(query_4,
                                                                            global_id=start_module[0],
                                                                            routing_="r",
                                                                            database_="neo4j")
    a = []
    for record in records_4:
        a += record["global_id"]
    basic_module = list(filter(lambda module: module in neo4j_connector.basic_ids, np.unique(a)))[0]
    dictionary = {'house_modules': np.unique(a),
                  'basic_module': basic_module,
                  'house_door': start_module[1],
                  'floor_name': neo4j_connector.objective_floor_name}
    houses_2_objective.append(dictionary)

# STEP 3.1.1 增加源户型的基本模块的周边关系（包括连通模块的名称，角度，和相对位置）
for house in houses_1_source:
    surrounding = []
    query_5 = ("MATCH p = (x:Basic_Module {global_id:$global_id})-[r:ACCESSES_1]-(y:Conjunctive_Module) "
               "RETURN y.global_id")
    records_5, summary_5, keys_5 = neo4j_connector.app.driver.execute_query(query_5,
                                                                            global_id=house['basic_module'],
                                                                            routing_="r",
                                                                            database_="neo4j")
    for r in records_5:
        query_6 = ("MATCH p = (x:Basic_Module {global_id:$global_id_1})-[r:CONNECTS]-(y {global_id:$global_id_2}) "
                   "RETURN r.angle, r.relative_position")
        records_6, summary_6, keys_6 = neo4j_connector.app.driver.execute_query(query_6,
                                                                                global_id_1=house['basic_module'],
                                                                                global_id_2=r['y.global_id'],
                                                                                routing_="r",
                                                                                database_="neo4j")

        dictionary = {'accessed_module': r['y.global_id'],
                      'angle': records_6[0]['r.angle'],
                      'relative_position': records_6[0]['r.relative_position']}
        surrounding.append(dictionary)
    house['surrounding'] = surrounding

# STEP 3.1.2 增加同楼层目标户型的基本模块的周边关系（包括连通模块的名称，角度，和相对位置）
for house in houses_1_objective:
    surrounding = []
    query_5 = ("MATCH p = (x:Basic_Module {global_id:$global_id})-[r:ACCESSES_1]-(y:Conjunctive_Module) "
               "RETURN y.global_id")
    records_5, summary_5, keys_5 = neo4j_connector.app.driver.execute_query(query_5,
                                                                            global_id=house['basic_module'],
                                                                            routing_="r",
                                                                            database_="neo4j")
    for r in records_5:
        query_6 = ("MATCH p = (x:Basic_Module {global_id:$global_id_1})-[r:CONNECTS]-(y {global_id:$global_id_2}) "
                   "RETURN r.angle, r.relative_position")
        records_6, summary_6, keys_6 = neo4j_connector.app.driver.execute_query(query_6,
                                                                                global_id_1=house['basic_module'],
                                                                                global_id_2=r['y.global_id'],
                                                                                routing_="r",
                                                                                database_="neo4j")

        dictionary = {'accessed_Module': r['y.global_id'],
                      'angle': records_6[0]['r.angle'],
                      'relative_position': records_6[0]['r.relative_position']}
        surrounding.append(dictionary)
    house['surrounding'] = surrounding

# STEP 3.1.3 增加其他楼层目标户型的基本模块的周边关系（包括连通模块的名称，角度，和相对位置）
for house in houses_2_objective:
    surrounding = []
    query_5 = ("MATCH p = (x:Basic_Module {global_id:$global_id})-[r:ACCESSES_2]-(y:Conjunctive_Module) "
               "RETURN y.global_id")
    records_5, summary_5, keys_5 = neo4j_connector.app.driver.execute_query(query_5,
                                                                            global_id=house['basic_module'],
                                                                            routing_="r",
                                                                            database_="neo4j")
    for r in records_5:
        query_6 = ("MATCH p = (x:Basic_Module {global_id:$global_id_1})-[r:CONNECTS]-(y {global_id:$global_id_2}) "
                   "RETURN r.angle, r.relative_position")
        records_6, summary_6, keys_6 = neo4j_connector.app.driver.execute_query(query_6,
                                                                                global_id_1=house['basic_module'],
                                                                                global_id_2=r['y.global_id'],
                                                                                routing_="r",
                                                                                database_="neo4j")

        dictionary = {'accessed_Module': r['y.global_id'],
                      'angle': records_6[0]['r.angle'],
                      'relative_position': records_6[0]['r.relative_position']}
        surrounding.append(dictionary)
    house['surrounding'] = surrounding

house_objective = houses_1_objective + houses_2_objective

for house in houses_1_source:
    print(house)
    house_obj = list(filter(lambda ho: ho['house_door'] == house['house_door'], house_objective))
    angles_tr = []
    angles_rc90 = []
    angles_rc180 = []
    angles_rc270 = []
    angles_hmi = []
    angles_vmi = []
    angles_rc90_hmi = []
    angles_rc90_vmi = []
    if len(house['house_modules']) > 1:
        for s in house['surrounding']:
            angle = s['angle']
            angle_tr = 0
            angle_rc90 = 0
            angle_rc180 = 0
            angle_rc270 = 0
            angle_hmi = 0
            angle_vmi = 0
            angle_rc90_hmi = 0
            angle_rc90_vmi = 0

            if -180 < angle <= 180:
                angle_tr = angle

            if -180 < angle < 0:
                angle_hmi = - angle - 180
            if 0 <= angle <= 180:
                angle_hmi = - angle + 180

            if -180 < angle < 180:
                angle_vmi = -angle
            if angle == 180:
                angle_vmi = 180

            if -180 < angle <= 0:
                angle_rc180 = angle + 180
            if 0 < angle <= 180:
                angle_rc180 = angle - 180

            if -180 < angle <= 90:
                angle_rc90 = angle + 90
                angle_rc270 = angle + 270
            if 90 < angle <= 180:
                angle_rc90 = angle - 270
                angle_rc270 = angle - 90

            if -180 < angle < 90:
                angle_rc90_hmi = - angle - 270
                angle_rc90_vmi = - angle - 90
            if 90 <= angle <= 180:
                angle_rc90_hmi = - angle + 90
                angle_rc90_vmi = - angle + 270

            angles_tr.append(angle_tr)
            angles_rc90.append(angle_rc90)
            angles_rc180.append(angle_rc180)
            angles_rc270.append(angle_rc270)
            angles_hmi.append(angle_hmi)
            angles_vmi.append(angle_vmi)
            angles_rc90_hmi.append(angle_rc90_hmi)
            angles_rc90_vmi.append(angle_rc90_vmi)
        for h in house_obj:
            transform = ''
            angles_obj = []
            for s in h['surrounding']:
                angles_obj.append(s['angle'])
            if set(angles_obj) == set(angles_tr):
                transform = "Translation"
            elif set(angles_obj) == set(angles_rc90):
                transform = "Translation + 90-degree Counterclockwise Rotation"
            elif set(angles_obj) == set(angles_rc180):
                transform = "Translation + 180-degree Counterclockwise Rotation"
            elif set(angles_obj) == set(angles_rc270):
                transform = "Translation + 270-degree Counterclockwise Rotation"
            elif set(angles_obj) == set(angles_hmi):
                transform = "Translation + Horizontal Mirror"
            elif set(angles_obj) == set(angles_vmi):
                transform = "Translation + Vertical Mirror"
            elif set(angles_obj) == set(angles_rc90_vmi):
                transform = "Translation + 90-degree Counterclockwise Rotation + Vertical Mirror"
            elif set(angles_obj) == set(angles_rc90_hmi):
                transform = "Translation + 90-degree Counterclockwise Rotation + Horizontal Mirror"
            h['transform'] = transform
    elif len(house['house_modules']) == 1:
        for h in house_obj:
            transform = 'Any'
            h['transform'] = transform

# for house in houses_1_source:
#     house_obj = list(filter(lambda ho: ho['house_door'] == house['house_door'], house_objective))
#     angles_tr = []
#     angles_rc90 = []
#     angles_rc180 = []
#     angles_rc270 = []
#     angles_ymi = []
#     angles_xmi = []
#     angles_rc90_xmi = []
#     angles_rc90_ymi = []
#     if len(house['house_modules']) > 1:
#         for s in house['surrounding']:
#             angle_tr = 0
#             angle_rc90 = 0
#             angle_rc180 = 0
#             angle_rc270 = 0
#             angle_ymi = 0
#             angle_xmi = 0
#             angle_rc90_xmi = 0
#             angle_rc90_ymi = 0
#             if s['relative_position'] == 'up&down':
#                 angle_tr = s['angle']
#                 angle_rc90 = s['angle'] + 90
#                 angle_rc180 = s['angle'] + 180
#                 if -180 < s['angle'] < -90:
#                     angle_rc270 = s['angle'] + 270
#                     angle_rc90_ymi = - s['angle'] - 270
#                 elif -90 < s['angle'] < 0:
#                     angle_rc270 = s['angle'] - 90
#                     angle_rc90_ymi = - s['angle'] + 90
#                 angle_xmi = - s['angle']
#                 angle_ymi = - s['angle'] - 180
#                 angle_rc90_xmi = - s['angle'] - 90
#             elif s['relative_position'] == 'down&up':
#                 angle_tr = s['angle']
#                 if 0 < s['angle'] < 90:
#                     angle_rc90 = s['angle'] + 90
#                     angle_rc90_xmi = - s['angle'] - 90
#                 elif 90 < s['angle'] < 180:
#                     angle_rc90 = s['angle'] - 270
#                     angle_rc90_xmi = - s['angle'] + 270
#                 angle_rc180 = s['angle'] - 180
#                 angle_rc270 = s['angle'] - 90
#                 angle_xmi = - s['angle']
#                 angle_ymi = 180 - s['angle']
#                 angle_rc90_ymi = - s['angle'] + 90
#             elif s['relative_position'] == 'left&right':
#                 angle_tr = s['angle']
#                 angle_rc90 = s['angle'] + 90
#                 if -90 < s['angle'] < 0:
#                     angle_rc180 = s['angle'] + 180
#                     angle_ymi = -180 - s['angle']
#                 elif 0 < s['angle'] < 90:
#                     angle_rc180 = s['angle'] - 180
#                     angle_ymi = 180 - s['angle']
#                 angle_rc270 = s['angle'] - 90
#                 angle_xmi = - s['angle']
#                 angle_rc90_xmi = - s['angle'] - 90
#                 angle_rc90_ymi = - s['angle'] + 90
#             elif s['relative_position'] == 'right&left':
#                 angle_tr = s['angle']
#                 if -180 < s['angle'] < -90:
#                     angle_rc90 = s['angle'] + 90
#                     angle_rc180 = s['angle'] + 180
#                     angle_rc270 = s['angle'] + 270
#                     angle_ymi = -180 - s['angle']
#                     angle_rc90_xmi = - s['angle'] - 90
#                     angle_rc90_ymi = - s['angle'] - 270
#                 elif 90 < s['angle'] < 180:
#                     angle_rc90 = s['angle'] - 270
#                     angle_rc180 = s['angle'] - 180
#                     angle_rc270 = s['angle'] - 90
#                     angle_ymi = 180 - s['angle']
#                     angle_rc90_xmi = - s['angle'] + 270
#                     angle_rc90_ymi = - s['angle'] + 90
#                 angle_xmi = - s['angle']
#
#             angles_tr.append(angle_tr)
#             angles_rc90.append(angle_rc90)
#             angles_rc180.append(angle_rc180)
#             angles_rc270.append(angle_rc270)
#             angles_ymi.append(angle_ymi)
#             angles_xmi.append(angle_xmi)
#             angles_rc90_xmi.append(angle_rc90_xmi)
#             angles_rc90_ymi.append(angle_rc90_ymi)
#         for h in house_obj:
#             transform = ''
#             angles_obj = []
#             for s in h['surrounding']:
#                 angles_obj.append(s['angle'])
#             if set(angles_obj) == set(angles_tr):
#                 transform = 'translation'
#             elif set(angles_obj) == set(angles_rc90):
#                 transform = "Rotation 90 degrees counterclockwise"
#             elif set(angles_obj) == set(angles_rc180):
#                 transform = "Rotation 180 degrees counterclockwise"
#             elif set(angles_obj) == set(angles_rc270):
#                 transform = "Rotation 270 degrees counterclockwise"
#             elif set(angles_obj) == set(angles_ymi):
#                 transform = "Mirroring about y-axis"
#             elif set(angles_obj) == set(angles_xmi):
#                 transform = "Mirroring about x-axis"
#             elif set(angles_obj) == set(angles_rc90_xmi):
#                 transform = "Rotation 90 degrees counterclockwise and then Mirroring about x-axis"
#             elif set(angles_obj) == set(angles_rc90_ymi):
#                 transform = "Rotation 90 degrees counterclockwise and then Mirroring about y-axis"
#             h['transform'] = transform
#     elif len(house['house_modules']) == 1:
#         for h in house_obj:
#             transform = 'Any'
#             h['transform'] = transform


neo4j_connector.app.close()
