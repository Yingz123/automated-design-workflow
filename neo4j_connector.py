import neo4j_driver
import data_extractor

# Connect to the Neo4j database
scheme = "bolt"
host_name = "localhost"
port = 7687
url = f"{scheme}://{host_name}:{port}"
username = "neo4j"
password = "Xyz0531!!"
app = neo4j_driver.App(url, username, password)

# user-input data
file_name = 'MA-Case Study.ifc'
source_floor_name = 'House Plan1'
objective_floor_name = 'House Plan2'
module_floor_name = 'module'
# (Simply enter the same fields from the family type names of modules)
tube_module_type_name = 'Tube module'
corridor_module_type_name = 'Corridor module'
basic_module_type_name = 'Basic module'
conjunctive_module_type_name = 'Conjunctive module'
fitted_bathroom_type_name = 'Fitted Bathroom'
fitted_kitchen_type_name = 'none'
wall_width_min = 200
wall_width_max = 200

# all ifc data
ifc_info = data_extractor.DataExtractor(file_name, module_floor_name, tube_module_type_name, corridor_module_type_name,
                                        basic_module_type_name, conjunctive_module_type_name, fitted_bathroom_type_name,
                                        fitted_kitchen_type_name)

# view-related data
elevation_1 = ifc_info.getElevation(source_floor_name)
elevation_2 = ifc_info.getElevation(objective_floor_name)

# module-related data
tube_module_data = ifc_info.getTubeModuleData()
corridor_module_data = ifc_info.getCorridorModuleData()
basic_module_data = ifc_info.getBasicModuleData()
conjunct_module_data = ifc_info.getConjunctiveModuleData()

basic_ids = []
for b in basic_module_data:
    basic_ids.append(b['IFC_GUID'])
conjunctive_ids = []
for c in conjunct_module_data:
    conjunctive_ids.append(c['IFC_GUID'])


