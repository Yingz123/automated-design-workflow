import neo4j_connector

# connectivity
tube_module_connectivity = neo4j_connector.ifc_info.getTubeModuleConnectivity()
tube_basic_module_connectivity = neo4j_connector.ifc_info.getTubeBasicModuleConnectivity()
tube_conjunct_module_connectivity = neo4j_connector.ifc_info.getTubeConjunctModuleConnectivity()
tube_corridor_module_connectivity = neo4j_connector.ifc_info.getTubeCorridorModuleConnectivity()

corridor_module_connectivity = neo4j_connector.ifc_info.getCorridorModuleConnectivity()
corridor_basic_module_connectivity = neo4j_connector.ifc_info.getCorridorBasicModuleConnectivity()
corridor_conjunct_module_connectivity = neo4j_connector.ifc_info.getCorridorConjunctModuleConnectivity()

basic_module_connectivity = neo4j_connector.ifc_info.getBasicModuleConnectivity()
basic_conjunct_module_connectivity = neo4j_connector.ifc_info.getBasicConjunctModuleConnectivity()
conjunct_module_connectivity = neo4j_connector.ifc_info.getConjunctModuleConnectivity()

# accessibility
tube_corridor_module_accessibility_1 = neo4j_connector.ifc_info.getTubeCorridorModuleAccessibility(neo4j_connector.elevation_1)
corridor_module_house_accessibility_1 = neo4j_connector.ifc_info.getCorridorHouseAccessibility(neo4j_connector.elevation_1)
house_accessibility_1 = neo4j_connector.ifc_info.getHouseAccessibility(neo4j_connector.elevation_1,
                                                                       neo4j_connector.wall_width_min, neo4j_connector.wall_width_max)

tube_corridor_module_accessibility_2 = neo4j_connector.ifc_info.getTubeCorridorModuleAccessibility(neo4j_connector.elevation_2)
corridor_module_house_accessibility_2 = neo4j_connector.ifc_info.getCorridorHouseAccessibility(neo4j_connector.elevation_2)
house_accessibility_2 = neo4j_connector.ifc_info.getHouseAccessibility(neo4j_connector.elevation_2,
                                                                       neo4j_connector.wall_width_min,
                                                                       neo4j_connector.wall_width_max)

# Add all the modules/nodes and their properties to the Neo4j database
for tube_module in neo4j_connector.tube_module_data:
    neo4j_connector.app.add_tube_module(tube_module['IFC_GUID'])

for corridor_module in neo4j_connector.corridor_module_data:
    neo4j_connector.app.add_corridor_module(corridor_module['IFC_GUID'])

for basic_module in neo4j_connector.basic_module_data:
    neo4j_connector.app.add_basic_module(basic_module['IFC_GUID'],
                                         neo4j_connector.ifc_info.getComponentsOfModule(basic_module,
                                                                                        neo4j_connector.elevation_1,
                                                                                        neo4j_connector.wall_width_min))

for conjunctive_module in neo4j_connector.conjunct_module_data:
    neo4j_connector.app.add_conjunctive_module(conjunctive_module['IFC_GUID'],
                                               neo4j_connector.ifc_info.getComponentsOfModule(conjunctive_module,
                                                                                              neo4j_connector.elevation_1,
                                                                                              neo4j_connector.wall_width_min))

# Add all the connectivity to the Neo4j database: "CONNECTS"
for i in range(len(tube_module_connectivity)):
    if tube_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_tube_connectivity(tube_module_connectivity[i][0],
                                                  tube_module_connectivity[i][1])

for i in range(len(tube_basic_module_connectivity)):
    if tube_basic_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_tube_basic_connectivity(tube_basic_module_connectivity[i][0],
                                                        tube_basic_module_connectivity[i][1])

for i in range(len(tube_conjunct_module_connectivity)):
    if tube_conjunct_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_tube_conjunct_connectivity(tube_conjunct_module_connectivity[i][0],
                                                           tube_conjunct_module_connectivity[i][1])

for i in range(len(tube_corridor_module_connectivity)):
    if tube_corridor_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_tube_corridor_connectivity(tube_corridor_module_connectivity[i][0],
                                                           tube_corridor_module_connectivity[i][1])

for i in range(len(corridor_module_connectivity)):
    if corridor_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_corridor_connectivity(corridor_module_connectivity[i][0],
                                                      corridor_module_connectivity[i][1])

for i in range(len(corridor_basic_module_connectivity)):
    if corridor_basic_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_corridor_basic_connectivity(corridor_basic_module_connectivity[i][0],
                                                            corridor_basic_module_connectivity[i][1])

for i in range(len(corridor_conjunct_module_connectivity)):
    if corridor_conjunct_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_corridor_conjunct_connectivity(corridor_conjunct_module_connectivity[i][0],
                                                               corridor_conjunct_module_connectivity[i][1])

for i in range(len(basic_module_connectivity)):
    if basic_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_basic_connectivity(basic_module_connectivity[i][0],
                                                   basic_module_connectivity[i][1],
                                                   basic_module_connectivity[i][6],
                                                   basic_module_connectivity[i][3])

for i in range(len(basic_conjunct_module_connectivity)):
    if basic_conjunct_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_basic_conjunct_connectivity(basic_conjunct_module_connectivity[i][0],
                                                            basic_conjunct_module_connectivity[i][1],
                                                            basic_conjunct_module_connectivity[i][6],
                                                            basic_conjunct_module_connectivity[i][3])

for i in range(len(conjunct_module_connectivity)):
    if conjunct_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_conjunct_connectivity(conjunct_module_connectivity[i][0],
                                                      conjunct_module_connectivity[i][1],
                                                      conjunct_module_connectivity[i][6],
                                                      conjunct_module_connectivity[i][3])

# Add all the accessibility for the source floor："ACCESSES_1"
for i in range(len(tube_module_connectivity)):
    if tube_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_tube_accessibility_1(tube_module_connectivity[i][0],
                                                     tube_module_connectivity[i][1])

for i in range(len(tube_corridor_module_accessibility_1)):
    neo4j_connector.app.add_tube_corridor_accessibility_1(tube_corridor_module_accessibility_1[i][0],
                                                          tube_corridor_module_accessibility_1[i][1])

for i in range(len(corridor_module_connectivity)):
    if corridor_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_corridor_accessibility_1(corridor_module_connectivity[i][0],
                                                         corridor_module_connectivity[i][1])

for i in range(len(corridor_module_house_accessibility_1)):
    if corridor_module_house_accessibility_1[i][1] in neo4j_connector.basic_ids:
        neo4j_connector.app.add_corridor_basic_accessibility_1(corridor_module_house_accessibility_1[i][0],
                                                               corridor_module_house_accessibility_1[i][1],
                                                               corridor_module_house_accessibility_1[i][3])
    elif corridor_module_house_accessibility_1[i][1] in neo4j_connector.conjunctive_ids:
        neo4j_connector.app.add_corridor_conjunct_accessibility_1(corridor_module_house_accessibility_1[i][0],
                                                                  corridor_module_house_accessibility_1[i][1],
                                                                  corridor_module_house_accessibility_1[i][3])

for i in range(len(house_accessibility_1)):
    if house_accessibility_1[i][0] in neo4j_connector.conjunctive_ids:
        connectivity = list(filter(lambda cc: cc[0] == house_accessibility_1[i][0] and
                                              cc[1] == house_accessibility_1[i][1],
                                   conjunct_module_connectivity))[0]

        neo4j_connector.app.add_conjunct_accessibility_1(house_accessibility_1[i][0],
                                                         house_accessibility_1[i][1],
                                                         neo4j_connector.ifc_info.getWallsOfCoEdge(connectivity,
                                                                                                   neo4j_connector.elevation_1,
                                                                                                   neo4j_connector.wall_width_min))

    elif house_accessibility_1[i][0] \
            in neo4j_connector.basic_ids and house_accessibility_1[i][1] in neo4j_connector.conjunctive_ids:
        connectivity = list(filter(lambda cc: cc[0] == house_accessibility_1[i][0] and
                                              cc[1] == house_accessibility_1[i][1],
                                   basic_conjunct_module_connectivity))[0]

        neo4j_connector.app.add_basic_conjunct_accessibility_1(house_accessibility_1[i][0],
                                                               house_accessibility_1[i][1],
                                                               neo4j_connector.ifc_info.getWallsOfCoEdge(connectivity,
                                                                                                         neo4j_connector.elevation_1,
                                                                                                         neo4j_connector.wall_width_min))

# Add all the accessibility for the objective floor："ACCESSES_2"
for i in range(len(tube_module_connectivity)):
    if tube_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_tube_accessibility_2(tube_module_connectivity[i][0], tube_module_connectivity[i][1])

for i in range(len(tube_corridor_module_accessibility_2)):
    neo4j_connector.app.add_tube_corridor_accessibility_2(tube_corridor_module_accessibility_2[i][0],
                                                          tube_corridor_module_accessibility_2[i][1])

for i in range(len(corridor_module_connectivity)):
    if corridor_module_connectivity[i][2] == 1:
        neo4j_connector.app.add_corridor_accessibility_2(corridor_module_connectivity[i][0],
                                                         corridor_module_connectivity[i][1])

for i in range(len(corridor_module_house_accessibility_2)):
    if corridor_module_house_accessibility_2[i][1] in neo4j_connector.basic_ids:
        neo4j_connector.app.add_corridor_basic_accessibility_2(corridor_module_house_accessibility_2[i][0],
                                                               corridor_module_house_accessibility_2[i][1],
                                                               corridor_module_house_accessibility_2[i][3])

    elif corridor_module_house_accessibility_2[i][1] in neo4j_connector.conjunctive_ids:
        neo4j_connector.app.add_corridor_conjunct_accessibility_2(corridor_module_house_accessibility_2[i][0],
                                                                  corridor_module_house_accessibility_2[i][1],
                                                                  corridor_module_house_accessibility_2[i][3])

for i in range(len(house_accessibility_2)):
    if house_accessibility_2[i][0] in neo4j_connector.conjunctive_ids:
        neo4j_connector.app.add_conjunct_accessibility_2(house_accessibility_2[i][0], house_accessibility_2[i][1])
    elif house_accessibility_2[i][0] in neo4j_connector.basic_ids and house_accessibility_2[i][1] in neo4j_connector.conjunctive_ids:
        neo4j_connector.app.add_basic_conjunct_accessibility_2(house_accessibility_2[i][0], house_accessibility_2[i][1])

# Close the connection to the Neo4j Database
neo4j_connector.app.close()
